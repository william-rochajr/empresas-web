import React, { useState, useEffect } from "react";
import EnterprisesService from "../../services/providers/enterprises.provider";

import EnterprisesCard from "./enterprisesCard";
import Header from "../../components/header";
import Loader from "../../components/loader";

import logo from "../../assets/logo-nav@2x.png";
import search from "../../assets/ic-search-copy@3x.png";
import close from "../../assets/ic-close@2x.png";

import "./style.css";

const Home = () => {
  const [openSearch, setOpenSearch] = useState(false);
  const [enterprisesData, setEnterprisesData] = useState([]);
  const [loading, setLoading] = useState(false);

  const [nameFilter, setNameFilter] = useState("");
  const [InitialSearch, setInitialSearch] = useState(true);
  const [noReturnFilter, setNoReturnFilter] = useState(false);

  useEffect(() => {
    // _getEnterprises();
  }, []);

  const _getEnterprises = () => {
    setLoading(true);
    const enterpriseService = new EnterprisesService();
    const enterpriseID = "";
    enterpriseService.getEnterprises(
      enterpriseID,
      nameFilter,
      async (response) => {
        let enterprisesArray = await JSON.parse(response.request.response);
        setEnterprisesData(enterprisesArray.enterprises);
        if (
          enterprisesArray.enterprises.length > 0 &&
          enterprisesArray.enterprises.length !== null
        ) {
          setNoReturnFilter(false);
        } else {
          setNoReturnFilter(true);
        }
        setLoading(false);
      },
      (error) => {
        setEnterprisesData([]);
        setLoading(false);
        // toast.error(error);
      }
    );
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      _getEnterprises();
    }
  };

  const msgBody = (isShow, msg) => (
    <div id="home-msg-detail" style={{ display: isShow ? "block" : "none" }}>
      {msg ? (
        <span>Clique na busca para iniciar.</span>
      ) : (
        <span> "Nenhuma empresa foi encontrada para a busca realizada."</span>
      )}
    </div>
  );

  return (
    <>
      <main className="home-container">
        <Header>
          <div className="home-div-aux"></div>
          <div
            className="home-logo-container"
            style={{
              opacity: openSearch ? "0" : "1",
              width: openSearch ? "0" : "auto",
            }}
          >
            <img className="login-logo" src={logo} alt="logo" />
          </div>
          <div
            className="home-search-container"
            style={{
              borderBottom: openSearch ? "1px solid white" : "none",
              flex: openSearch ? "1" : "0",
            }}
          >
            <div>
              <img
                className="home-search-icon"
                src={search}
                onClick={() => {
                  setOpenSearch(true);
                  setInitialSearch(false);
                  _getEnterprises();
                }}
                alt="search"
              />
              {nameFilter.length <= 0 && (
                <span
                  className="home-placeholder"
                  style={{
                    opacity: openSearch ? "1" : "0",
                  }}
                >
                  Pesquisar
                </span>
              )}
            </div>
            <input
              className="home-input-search"
              onChange={(e) => {
                setNameFilter(e.target.value);
                console.log("target", e.target.value);
              }}
              onKeyDown={handleKeyDown}
            />
            <img
              className="home-close-icon"
              style={{
                display: openSearch ? "block" : "none",
              }}
              src={close}
              onClick={() => {
                setOpenSearch(false);
              }}
              alt="search"
            />
          </div>
        </Header>
        <section className="home-body-container">
          {msgBody(InitialSearch, true)}
          {msgBody(noReturnFilter, false)}
          {loading ? <Loader /> : <EnterprisesCard value={enterprisesData} />}
        </section>
      </main>
    </>
  );
};

export default Home;
