import React from "react";
import list from "../../assets/img-e-1-lista.png";
import { Link } from "react-router-dom";

const EnterprisesCard = ({ value }) => {
  return (
    <>
      <section className="home-list-grid">
        {value.map((item) => (
          <Link to={`/detail/${item.id}`}>
            <div className="home-card-list">
              <div style={{ flex: "1" }}>
                <img
                  src={
                    item.photo !== null
                      ? `http://empresas.ioasys.com.br${item.photo}`
                      : list
                  }
                  alt="list"
                  className="home-card-img"
                ></img>
              </div>
              <div className="home-card-description">
                <div className="home-card-empresa">{item.enterprise_name}</div>
                <div className="home-card-negocio">
                  {item.enterprise_type.enterprise_type_name}
                </div>
                <div className="home-card-pais">{item.country}</div>
              </div>
            </div>
          </Link>
        ))}
      </section>
    </>
  );
};

export default EnterprisesCard;
