import React, { useState } from "react";

import { useHistory } from "react-router-dom";
import AuthService from "../../services/security/auth";
import Loader from "../../components/loader";

import logo from "../../assets/logo-home.png";
import padLock from "../../assets/ic-cadeado@2x.png";
import email from "../../assets/ic-email@2x.png";
import eyed from "../../assets/icons8-eye-30.png";
import warning from "../../assets/warning.png";

import { Formik, Field, Form, ErrorMessage } from "formik";
import { LoginSchema } from "./validation.js";

import "./style.css";

const Login = () => {
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassoword] = useState(false);

  const initialValues = { email: "", password: "" };

  const history = useHistory();
  const authService = new AuthService();

  const doLogin = (values) => {
    setLoading(true);
    const form = new FormData();
    for (var key in values) {
      form.append(key, values[key]);
    }
    authService.login(
      form,
      (response) => {
        console.log("Login efetuado com sucesso!");
        setLoading(false);
        history.push("/home");
      },
      (error) => {
        setLoading(false);
      }
    );
  };

  return (
    <div className="login-container">
      {loading && (
        <div className="login-loader-bg">
          <Loader sizeLoad="110" />
        </div>
      )}
      <Formik
        initialValues={initialValues}
        validationSchema={LoginSchema}
        enableReinitialize={true}
        onSubmit={(values, actions) => {
          actions.setSubmitting(true);
          doLogin(values);
        }}
      >
        {() => (
          <Form className="login-form" autoComplete={"off"}>
            <img className="login-logo" src={logo} alt="logo" />
            <div style={{ padding: "0px 2%" }}>
              <h1 className="login-title">BEM-VINDO AO EMPRESAS</h1>
              <div className="login-subtitle">
                Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc
                accumsan.
              </div>
            </div>
            <section className="login-campos-container">
              <div className="login-input-container">
                <img className="login-icons" src={email} alt="login-icons" />
                <Field
                  id="my"
                  className="form-control"
                  type="email"
                  name="email"
                  placeholder="E-mail"
                />
                <ErrorMessage name="email">
                  {() => (
                    <img
                      className="login-icon-warning"
                      src={warning}
                      alt="warning"
                    />
                  )}
                </ErrorMessage>
              </div>
              <ErrorMessage name="email">
                {(msg) => <div className="login-erro-msg">{msg}</div>}
              </ErrorMessage>
              <div className="login-input-container">
                <img className="login-icons" src={padLock} alt="padlock" />
                <Field
                  type={showPassword ? "text" : "password"}
                  className="form-control"
                  name="password"
                  autoComplete={"off"}
                  aria-describedby="validationServer05Feedback"
                />
                <ErrorMessage name="password">
                  {() => (
                    <img
                      className="login-icon-warning"
                      src={warning}
                      alt="warning"
                    />
                  )}
                </ErrorMessage>
                <img
                  className="login-eyed-icon"
                  src={eyed}
                  onClick={() => setShowPassoword(!showPassword)}
                  alt="eyed"
                />
              </div>
              <ErrorMessage name="password">
                {(msg) => <div className="login-erro-msg">{msg}</div>}
              </ErrorMessage>
            </section>
            <button className="login-btn" type="submit">
              ENTRAR
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Login;
