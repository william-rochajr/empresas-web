import * as Yup from 'yup';

export const LoginSchema = Yup.object().shape({
	email: Yup.string()
		.email('Email inválido')
		.required('Campo obrigatório.'),
	password: Yup.string()
		.min(3, 'Senha muito curta!')
		.max(10, 'Senha muito longa!')
		.required('Campo obrigatório.'),
});

export const RecuperarSenhaSchema = Yup.object().shape({
	email: Yup.string()
		.email('Email inválido')
		.required('Campo obrigatório.'),
});

export const CriarSenhaSchema = Yup.object().shape({
	password: Yup.string()
		.min(3, 'Senha muito curta!')
		.max(10, 'Senha muito longa!')
		.required('Campo obrigatório.'),
});
