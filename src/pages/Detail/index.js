import React, { useEffect, useState } from "react";

import EnterprisesService from "../../services/providers/enterprises.provider";
import { useHistory } from "react-router-dom";
import Header from "../../components/header";
import Loader from "../../components/loader";

import LeftArrow from "../../assets/left-arrow.svg";
import list from "../../assets/img-e-1-lista.png";

import "./style.css";

const Detail = (props) => {
  const [enterpriseDetail, setEnterpriseDetail] = useState({});
  const [loading, setLoading] = useState(false);
  let history = useHistory();

  useEffect(() => {
    _getEnterprisesDetail();
  }, []);

  const _getEnterprisesDetail = () => {
    const { id } = props.match.params;
    setLoading(true);
    const enterpriseService = new EnterprisesService();
    const enterpriseID = id;
    const nameFilter = "";
    enterpriseService.getEnterprises(
      enterpriseID,
      nameFilter,
      (response) => {
        if (response) {
          let enterprisesArray = JSON.parse(response.request.response);
          if (enterprisesArray.success) {
            setEnterpriseDetail(enterprisesArray);
          }
          setLoading(false);
        }
      },
      (error) => {
        // setEnterprisesData([]);
        setLoading(false);
        // toast.error(error);
      }
    );
  };
  return (
    <>
      <div className="detail-container">
        <Header>
          <div style={{ display: "flex", alignItems: "center" }}>
            <div onClick={() => history.goBack()}>
              <img
                id="detail-left-icon"
                src={LeftArrow}
                alt="left-arrow"
                style={{ width: "60px" }}
              />
            </div>
            <span className="detail-header-title">
              {enterpriseDetail.success &&
                enterpriseDetail.enterprise.enterprise_name}
            </span>
          </div>
        </Header>
        <section className="detail-body-container">
          {!loading ? (
            <div className="detail-list-container">
              {enterpriseDetail.success && (
                <div className="detail-card-list">
                  <div className="detail-card-img-container">
                    <img
                      src={
                        enterpriseDetail.photo !== null
                          ? `http://empresas.ioasys.com.br${enterpriseDetail.enterprise.photo}`
                          : list
                      }
                      alt="list"
                      className="detail-card-img"
                    ></img>
                  </div>
                  <div className="detail-card-description">
                    {console.log(
                      "ente",
                      enterpriseDetail.enterprise.enterprise_name
                    )}
                    <div className="detail-card-negocio">
                      {enterpriseDetail.enterprise.description}
                    </div>
                  </div>
                </div>
              )}
            </div>
          ) : (
            <Loader />
          )}
        </section>
      </div>
    </>
  );
};

export default Detail;
