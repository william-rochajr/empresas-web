import HttpService from "../config/http";

export default class EnterpriseService {
  constructor(props) {
    this.api = new HttpService(props);
  }

  getEnterprises = (id, name, success, errorCallback) => {
    console.log(`/enterprises/${id}?enterprise_types=&name=${name}`);
    this.api
      .get(`/enterprises/${id}?name=${name}`)
      .then((result) => {
        if (result.status === 200) {
          console.log("enterprise result", result);
          success(result);
        }
      })
      .catch((error) => errorCallback(error));
  };
}
