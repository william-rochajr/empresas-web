import { HttpStatus } from "../../utils/constants/http-status";
import axios from "axios";
import environment from "./environment";

export default class HttpService {
  constructor(props) {
    const tokenAcess = localStorage.getItem("tokenAcess");
    const clientToken = localStorage.getItem("clientToken");
    const uidEmail = localStorage.getItem("uidEmail");
    const service = axios.create({
      baseURL: environment.baseUrl[environment.currentEnvironment],
      headers: {
        "Content-Type": "application/json",
        "access-token": tokenAcess,
        client: clientToken,
        uid: uidEmail,
      },
    });

    if (tokenAcess) {
      service.defaults.headers.common.Authorization = `access-token ${tokenAcess}`;
    }
    if (clientToken) {
      service.defaults.headers.common.Authorization = `client ${clientToken}`;
    }
    if (uidEmail) {
      service.defaults.headers.common.Authorization = `uid ${uidEmail}`;
    }

    service.interceptors.response.use(this.handleSuccess, this.handleError);

    this.service = service;
    this.props = props;
  }

  loadToken(newToken) {
    this.service.defaults.headers.common.Authorization = `access-token ${newToken.headers["access-token"]}`;
    this.service.defaults.headers.common.Authorization = `client ${newToken.headers.client}`;
    this.service.defaults.headers.common.Authorization = `uid ${newToken.headers.uid}`;
  }

  handleSuccess(response) {
    return response;
  }

  handleError = (error) => {
    if (error.response) {
      switch (error.response.status) {
        case HttpStatus.UNAUTHORIZED:
          const token = localStorage.getItem("token");
          if (token) {
            localStorage.removeItem("token");
            localStorage.removeItem("token");
          }
          return error;
        default:
          return error;
      }
    }
    Promise.reject(error);
  };

  redirectTo = (path) => {
    this.props.history.push(path);
  };

  get(path) {
    const url =
      environment.baseUrl[environment.currentEnvironment].urlApi + path;
    return this.service.get(url);
  }

  put(path, payload) {
    const url =
      environment.baseUrl[environment.currentEnvironment].urlApi + path;
    return this.service.put(url, payload);
  }

  putFile(path, payload) {
    const url =
      environment.baseUrl[environment.currentEnvironment].urlApi + path;
    return this.service.put(url, payload, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  }

  patch(path, payload) {
    const url =
      environment.baseUrl[environment.currentEnvironment].urlApi + path;
    return this.service.patch(url, payload);
  }

  patchFile(path, payload) {
    const url =
      environment.baseUrl[environment.currentEnvironment].urlApi + path;
    return this.service.patch(url, payload, {
      headers: {
        "access-token": "multipart/form-data",
      },
    });
  }

  post(path, payload) {
    const url =
      environment.baseUrl[environment.currentEnvironment].urlApi + path;

    return this.service.post(url, payload);
  }

  postFile(path, payload) {
    const url =
      environment.baseUrl[environment.currentEnvironment].urlApi + path;
    return this.service.post(url, payload, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  }

  delete(path) {
    const url =
      environment.baseUrl[environment.currentEnvironment].urlApi + path;
    return this.service.delete(url);
  }
}
