/* Configuração das rotas do APP para comunicação com a API */
export default {
  currentEnvironment: "dev",
  baseUrl: {
    dev: {
      urlApi: "https://empresas.ioasys.com.br/api/v1",
    },
    homolog: {
      urlApi: "https://empresas.ioasys.com.br/api/v1",
    },
    prod: {
      urlApi: "https://empresas.ioasys.com.br/api/v1",
    },
  },
};
