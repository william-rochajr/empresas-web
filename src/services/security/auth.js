import HttpService from "../config/http";

export default class AuthService {
  constructor(props) {
    this.api = new HttpService(props);
  }

  saveInfo = (data) => {
    localStorage.setItem("tokenAcess", data.headers['access-token']);
    localStorage.setItem("clientToken", data.headers.client);
    localStorage.setItem("uidEmail", data.headers.uid);
    this.api.loadToken(data);
  };

  getCurrentUser = () => localStorage.getItem("token");

  login = (userForm, success, errorCallback) => {
    this.api
      .post("/users/auth/sign_in", userForm)
      .then((result) => {
        console.log("resultado", result.status);
        if (result.status === 200) {
          this.saveInfo(result);
          success(result);
        } else {
          if (result.response.status === 401) {
            throw result.response.data.detail;
          } else {
            const responseError = "Erro ao efetuar o login!";
            throw responseError;
          }
        }
      })
      .catch((error) => errorCallback(error));
  };

  logout = () => {
    localStorage.removeItem("tokenAcess");
    localStorage.removeItem("clientToken");
    localStorage.removeItem("uidEmail");
  };
}
