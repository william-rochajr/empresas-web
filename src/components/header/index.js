import React from "react";
import "./style.css";

const Header = ({ children }) => {
  return (
    <header className="home-header-container">
      <div className="home-header-limiter">{children}</div>
    </header>
  );
};

export default Header;
