import React from "react";
import "./style.css";
const Loader = ({ sizeLoad }) => {
  return (
    <div
      className="home-loader"
      style={{ width: `${sizeLoad}px`, height: `${sizeLoad}px` }}
    ></div>
  );
};

export default Loader;
