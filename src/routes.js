import React, { Suspense } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// import 'global.css';

import Login from "./pages/Login";
import Home from "./pages/Home";
import Detail from "./pages/Detail";

const PublicRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      true ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/home",
          }}
        />
      )
    }
  />
);

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      setAuthorization("tokenAcess") &&
      setAuthorization("clientToken") &&
      setAuthorization("uidEmail") ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/",
          }}
        />
      )
    }
  />
);

export default function Routes() {
  return (
    <>
      <Suspense>
        <Switch>
          <PublicRoute exact path="/" component={Login} />
          <PrivateRoute exact path="/home" component={Home} />
          <PrivateRoute exact path="/detail/:id" component={Detail} />
        </Switch>
      </Suspense>
    </>
  );
}

const setAuthorization = (auth) =>
  localStorage.getItem(auth) &&
  localStorage.getItem(auth) !== null &&
  localStorage.getItem(auth) !== undefined;
